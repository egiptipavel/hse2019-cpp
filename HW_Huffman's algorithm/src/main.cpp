#include "../include/HuffmanArchiver.h"
#include "CLI.h"
#include "Exception.h"

#include <iostream>
#include <fstream>

int main(int argc, char* argv[]) {
	try {
		CLI cli(argc, argv);
		HuffmanArchiver archiver;
		if (cli.getTask() == TypeOfTask::Archive) {
			std::ifstream fileIn(cli.getFilename());
			std::ofstream fileOut(cli.getOutput(), std::ios::binary);

			if (!fileIn.is_open()) {
				throw Exception("Unable to open input file!\n");
			}
			if (!fileOut.is_open()) {
				throw Exception("Unable to open output file!\n");
			}

			archiver.compress(fileIn, fileOut);

			fileIn.close();
			fileOut.close();
		} else {
			std::ifstream fileIn(cli.getFilename(), std::ios::binary);
			std::ofstream fileOut(cli.getOutput());

			if (!fileIn.is_open()) {
				throw Exception("Unable to open input file!\n");
			}
			if (!fileOut.is_open()) {
				throw Exception("Unable to open output file!\n");
			}

			archiver.extract(fileIn, fileOut);

			fileIn.close();
			fileOut.close();
		}
	}
	catch(Exception& error) {
		std::cout << error.what();
	}
}
