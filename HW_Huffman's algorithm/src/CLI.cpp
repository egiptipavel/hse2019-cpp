#include "CLI.h"
#include "Exception.h"

#include <iostream>
#include <cstring>
#include <cstdlib>

CLI::CLI(int argc, char* argv[]) {
	if (argc < 6) {
		throw Exception("Not enough parameters!\n");
	}
	if (argc > 6) {
		throw Exception("Too many parameters!\n");
	}

	checkTask(argc, argv);

	TypeOfFile type = TypeOfFile::Input; 
	checkFileName("-f", "--file", argc, argv, type);

	type = TypeOfFile::Output;
	checkFileName("-o", "--output", argc, argv, type);
}

CLI::~CLI() {
	free(input);
	free(output);
}

void CLI::checkTask(int argc, char* argv[]) {
	for (int i = 1; i < argc; i++) {
		if (std::strcmp(argv[i], "-c") == 0) {
			task = TypeOfTask::Archive;
			break;
		}
		if (std::strcmp(argv[i], "-u") == 0) {
			task = TypeOfTask::Unarchive;
			break;
		}
		if (i == argc - 1) {
			throw Exception("Not correct input!\n");
		}
	}
}

void CLI::checkFileName(const char* firstType, const char* secondType, int argc, char* argv[], TypeOfFile type) {
	for (int i = 1; i < argc; i++) {
		if (((std::strcmp(argv[i], firstType) == 0) || (std::strcmp(argv[i], secondType) == 0))
			&& (i <= argc - 2)) {

			if (type == TypeOfFile::Input) {
				input = (char*)malloc(sizeof(char) * (std::strlen(argv[i + 1]) + 1));
				std::strcpy(input, argv[i + 1]);
			} else {
				output = (char*)malloc(sizeof(char) * (std::strlen(argv[i + 1]) + 1));
				std::strcpy(output, argv[i + 1]);
			}
			break;
		}
		if (i == argc - 1) {
			if (type == TypeOfFile::Output) free(input);
			throw Exception("Not correct input!\n");
		}
	}
}

const char* CLI::getFilename() const {
	return input;
}

const char* CLI::getOutput() const {
	return output;
}

TypeOfTask CLI::getTask() const {
	return task;
}
