#include "HuffmanTree.h"

#include <iostream>
#include <algorithm>

void HuffmanTree::check() {
	for (auto it : nodes) {
		std::cout << (int)it.getWord()[0] << ' ' << it.getSize() << '\n';
	}
	std::cout << '\n';
}

void HuffmanTree::addParent(std::string word, std::size_t size) {
	HuffmanNode node(word, size);
	nodes.push_back(node);
}

void HuffmanTree::addParent(std::string word, int left, int right, std::size_t size) {
	HuffmanNode node(word, left, right, size);
	nodes.push_back(node);
}

bool comp(HuffmanNode a, HuffmanNode b) {
	return a.getSize() < b.getSize();
}

void HuffmanTree::buildTree(bool needSort) {
	if (needSort) std::sort(nodes.begin(), nodes.end(), comp);

	for (std::size_t i = 0; i < nodes.size(); i++) {
		firstQueue.push(i);
	}

	while(firstQueue.size() + secondQueue.size() > 1) {
		int sNodes[2];
		for (std::size_t i = 0; i < 2; i++) {
			if (firstQueue.size() && secondQueue.size()) {
				sNodes[i] = 
				nodes[firstQueue.front()].getSize() < nodes[secondQueue.front()].getSize()
												? firstQueue.front() : secondQueue.front();

				nodes[firstQueue.front()].getSize() < nodes[secondQueue.front()].getSize()
												? firstQueue.pop() : secondQueue.pop();
			} else {
				if (firstQueue.size()) {
					sNodes[i] = firstQueue.front();
					firstQueue.pop();
				} else {
					sNodes[i] = secondQueue.front();
					secondQueue.pop();
				}
			}
		}

		addParent(nodes[sNodes[0]].getWord() + nodes[sNodes[1]].getWord(), 
				sNodes[0], sNodes[1], nodes[sNodes[0]].getSize() + nodes[sNodes[1]].getSize());
		secondQueue.push(nodes.size() - 1);
	}
	vSize = nodes.size();
}

void HuffmanTree::buildTableLetter(std::string str, int node) {
	if (nodes[node].getLeftChild() == -1 && nodes[node].getRightChild() == -1) {
		if (str == "") {
			str += '0';
		}
		table[nodes[node].getWord()] = str;
		return;
	}
	if (nodes[node].getLeftChild()  != -1) buildTableLetter(str + '0', nodes[node].getLeftChild());
	if (nodes[node].getRightChild() != -1) buildTableLetter(str + '1', nodes[node].getRightChild());
}

std::string HuffmanTree::getCode(const char letter) {
	std::string letterString;
	letterString += letter;
	return table[letterString];
}

const std::size_t HuffmanTree::getVsize() const {
	return vSize;
}

bool HuffmanTree::stringCount(std::string letter) const { 
	return table.count(letter);
}

char HuffmanTree::getLetter(std::string code) {
	return table[code][0];
}

const std::size_t HuffmanTree::saveTable(std::ostream& fileOut) const {
	int tableSize = 0;
	for (auto iterator : nodes) {
		if (iterator.getWord().size() > 1) break;
		fileOut << (unsigned char)iterator.getWord()[0] << " " << iterator.getSize() << " ";
		std::string str = std::to_string(iterator.getSize());
		tableSize += str.size() + 3;
	}
	return tableSize;
}

void HuffmanTree::buildTableCode(std::string str, int node) {
	if (nodes[node].getLeftChild() == -1 && nodes[node].getRightChild() == -1) {
		table[str] = nodes[node].getWord();
		return;
	}
	if (nodes[node].getLeftChild()  != -1) buildTableCode(str + '0', nodes[node].getLeftChild());
	if (nodes[node].getRightChild() != -1) buildTableCode(str + '1', nodes[node].getRightChild());
}
