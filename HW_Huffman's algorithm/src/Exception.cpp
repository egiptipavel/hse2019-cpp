#include "Exception.h"

#include <cstring>

Exception::Exception(const char* error_) {
	error = new char[strlen(error_) + 1];
    strcpy(error, error_);
}

Exception::~Exception() {
	delete[] error;
}

const char* Exception::what() const noexcept {
	return error;
}