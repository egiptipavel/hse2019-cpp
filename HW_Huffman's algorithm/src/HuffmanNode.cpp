#include "HuffmanNode.h"

HuffmanNode::HuffmanNode(std::string word_, std::size_t size_) :
	word(word_),
	leftChild(-1),
	rightChild(-1),
	size(size_)
	{}

HuffmanNode::HuffmanNode(std::string word_, int leftChild_, int rightChild_, std::size_t size_) :
	word(word_),
	leftChild(leftChild_),
	rightChild(rightChild_),
	size(size_)
	{}

bool HuffmanNode::operator<(const HuffmanNode& other) noexcept {
	return this->getSize() < other.getSize();
}

std::string HuffmanNode::getWord() const {
	return word;
}

int HuffmanNode::getLeftChild() const {
	return leftChild;
}

int HuffmanNode::getRightChild() const {
	return rightChild;
}

std::size_t HuffmanNode::getSize() const {
	return size;
}
