#include "HuffmanArchiver.h"

#include <iostream>
#include <cstdlib>
#include <fstream> 

void HuffmanArchiver::compress(std::istream& fileIn, std::ostream& fileOut) {
	std::vector<int> letters(256);

	unsigned char letter[1];
	fileIn.read((char*)letter, sizeof(unsigned char));
	if (fileIn.eof()) {
		std::cout << counterLetters << '\n'
				  << counterSizeOfData << '\n'
				  << counterSizeTable << '\n';
		return;
	}

	while (fileIn.good()) {
		letters[(int)letter[0]]++;
		counterLetters++;
		fileIn.read((char*)letter, sizeof(unsigned char));
	}
	for (std::size_t i = 0; i < 256; i++) {
		if (letters[i] > 0) {
			counterTypes++;
			std::string newLetter;
			newLetter += (unsigned char)i;
			tree.addParent(newLetter, letters[i]);
		}
	}

	countSizeFirstLine();

	tree.buildTree(true);
	tree.buildTableLetter("", tree.getVsize() - 1);

	fileOut << counterLetters << " " << counterTypes << " ";
	counterSizeTable += tree.saveTable(fileOut);

	fileIn.clear();
	fileIn.seekg(0, std::ios::beg);
	fileIn.read((char*)letter, sizeof(unsigned char));

	std::size_t posInString = 0, posInChar = 0;
	unsigned char next = '\0';
	while(fileIn.good()) {
		std::string codeLetter = tree.getCode(letter[0]);
		while (posInChar < 8 && posInString < codeLetter.size()) {
			next |= (codeLetter[posInString] == '0' ? 0 : 1);
			if (posInChar != 7) next <<= 1;
			posInString++;
			posInChar++;
		}
		if (posInString == codeLetter.size()) {
			posInString = 0;
			fileIn.read((char*)letter, 1);
		}
		if (posInChar == 8) {
			fileOut << next;
			posInChar = 0;
			next = (char)0;
			counterSizeOfData++;
		}
	}
	next <<= 7 - posInChar;
	fileOut << next;
	counterSizeOfData++;
	
	std::cout << counterLetters << '\n'
			  << counterSizeOfData << '\n'
			  << counterSizeTable << '\n';

}

void HuffmanArchiver::extract(std::istream& fileInput, std::ostream& fileOut) {
	fileInput >> counterLetters >> counterTypes;

	if (fileInput.eof()) {
		std::cout << counterSizeOfData << '\n'
				  << counterLetters << '\n'
				  << counterSizeTable << '\n';
		return;
	}

	unsigned char skipSpace[1];
	fileInput.read((char*)skipSpace, sizeof(char));

	countSizeFirstLine();

	for (int i = 0; i < counterTypes; i++) {
		char letter[1];
		fileInput.read((char*)letter, sizeof(char));
		counterSizeTable++;

		fileInput.read((char*)skipSpace, sizeof(char));
		counterSizeTable++;

		int size;
		fileInput >> size;
		int sizeOfNumber = std::to_string(size).size(); 
		counterSizeTable += sizeOfNumber;

		fileInput.read((char*)skipSpace, sizeof(char));
		counterSizeTable++;

		std::string str;
		str += letter[0];

		tree.addParent(str, -1, -1, size);
	}

	tree.buildTree(false);
	tree.buildTableCode("", tree.getVsize() - 1);

	char letter[1];
	fileInput.read((char*)letter, sizeof(unsigned char));
	counterSizeOfData++;

	std::string code;
	int readC = 0, posInChar = 0;
	while(fileInput.good() && readC < counterLetters) {
		while(!tree.stringCount(code) && posInChar < 8) {
			code += ((letter[0] & (unsigned char)128) ? '1' : '0');
			letter[0] <<= 1;
			posInChar++;
		}
		if (posInChar == 8) {
			fileInput.read((char*)letter, sizeof(unsigned char));
			counterSizeOfData++;
			posInChar = 0;
		}
		if (tree.stringCount(code)) {
			fileOut << tree.getLetter(code);
			readC++;
			code.clear();
		}
	}

	std::cout << counterSizeOfData << '\n'
			  << counterLetters << '\n'
			  << counterSizeTable << '\n';
}

void HuffmanArchiver::countSizeFirstLine() {
	counterSizeTable += std::to_string(counterLetters).size();
	counterSizeTable += std::to_string(counterTypes).size();
	counterSizeTable += 2; //two spaces
}