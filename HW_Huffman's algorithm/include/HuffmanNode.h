#ifndef HUFFMANNODE_H_
#define HUFFMANNODE_H_

#include <string>

class HuffmanNode {
public:
	HuffmanNode(std::string, std::size_t);
	HuffmanNode(std::string, int, int, std::size_t);

	bool operator<(const HuffmanNode&) noexcept;

	std::string getWord() const;
	int getLeftChild() const;
	int getRightChild() const;
	std::size_t getSize() const;

private:
	std::string word;
	int leftChild;
	int rightChild;
	std::size_t size;

};

#endif