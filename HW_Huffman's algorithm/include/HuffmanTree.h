#ifndef HUFFMANTREE_H_
#define HUFFMANTREE_H_

#include "HuffmanNode.h"

#include <unordered_map>
#include <queue>
#include <vector>
#include <fstream>
#include <memory>

class HuffmanTree {
public:
	HuffmanTree() = default;

	void check();

	void addParent(std::string, std::size_t);
	void addParent(std::string, int, int, std::size_t);

	void buildTree(bool);
	void buildTableLetter(std::string, int);
	void buildTableCode(std::string, int);
	const std::size_t saveTable(std::ostream&) const;

	const std::size_t getVsize() const;
	bool stringCount(std::string) const;
	std::string getCode(const char);
	char getLetter(std::string);

private:
	std::queue<int> firstQueue;
	std::queue<int> secondQueue;

	std::unordered_map<std::string, std::string> table;
	std::vector<HuffmanNode> nodes;

	std::size_t vSize;

};

#endif