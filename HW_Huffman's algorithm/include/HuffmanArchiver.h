#ifndef HUFFMANARCHIVER_H_
#define HUFFMANARCHIVER_H_

#include "HuffmanTree.h"
#include "HuffmanNode.h"

#include <fstream>

class HuffmanArchiver {
public:
	HuffmanArchiver() = default;

	void compress(std::istream&, std::ostream&);
	void extract(std::istream&, std::ostream&);

	void countSizeFirstLine();

private:
	HuffmanTree tree;

	std::size_t counterLetters = 0;
	std::size_t counterTypes = 0;
	std::size_t counterSizeOfData = 0;
	std::size_t counterSizeTable = 0;

};

#endif