#ifndef CLI_H
#define CLI_H

#include "Exception.h"

#include <string>

enum class TypeOfTask {
	Archive,
	Unarchive
};

enum class TypeOfFile {
	Input,
	Output
};

class CLI {
public:
	CLI(int, char**);
	~CLI();

	void checkTask(int argc, char**);
	void checkFileName(const char*, const char*, int, char**, TypeOfFile);

	const char* getFilename() const;
	const char* getOutput() const;
	TypeOfTask getTask() const;

private:
	char* input;
	char* output;
	TypeOfTask task;

};

#endif