#ifndef EXCEPTION_H
#define EXCEPTION_H

#include <string>

class Exception : std::exception {
public:
	Exception(const char*);
	~Exception();

	const char* what() const noexcept override;

private:
	char* error;
};

#endif