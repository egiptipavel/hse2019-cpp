#include "../include/HuffmanTree.h"
#include "../src/HuffmanTree.cpp"
#include "doctest.h"

TEST_CASE("test 1 letter") {
	HuffmanTree tree;

	tree.addParent("a", 1);

	tree.buildTree(true);
	CHECK(tree.getVsize() == 1);

	tree.buildTableLetter("" , tree.getVsize() - 1);
	CHECK(tree.getCode('a') == "0");

	CHECK(tree.stringCount("a") == true);
	CHECK(tree.stringCount("b") == false);
}

TEST_CASE("test building tree for archiving") {
	HuffmanTree tree;

	tree.addParent("a", 1);
	tree.addParent("b", 1);
	tree.addParent("c", 1);
	tree.addParent("d", 1);
	tree.addParent("e", 1);
	tree.addParent("f", 1);
	tree.addParent("g", 1);
	tree.addParent("h", 1);

	tree.buildTree(true);
	CHECK(tree.getVsize() == 15);

	tree.buildTableLetter("", tree.getVsize() - 1);
	CHECK(tree.getCode('a') == "000");
	CHECK(tree.getCode('b') == "001");
	CHECK(tree.getCode('c') == "010");
	CHECK(tree.getCode('d') == "011");
	CHECK(tree.getCode('e') == "100");
	CHECK(tree.getCode('f') == "101");
	CHECK(tree.getCode('g') == "110");
	CHECK(tree.getCode('h') == "111");

	CHECK(tree.stringCount("a") == true);
	CHECK(tree.stringCount("l") == false);
}

TEST_CASE("test building tree for unarchiving") {
	HuffmanTree tree;

	tree.addParent("a", 1);
	tree.addParent("b", 1);
	tree.addParent("c", 1);
	tree.addParent("d", 1);
	tree.addParent("e", 1);
	tree.addParent("f", 1);
	tree.addParent("g", 1);
	tree.addParent("h", 1);

	tree.buildTree(true);
	CHECK(tree.getVsize() == 15);

	tree.buildTableCode("", tree.getVsize() - 1);
	CHECK(tree.getLetter("000") == 'a');
	CHECK(tree.getLetter("001") == 'b');
	CHECK(tree.getLetter("010") == 'c');
	CHECK(tree.getLetter("011") == 'd');
	CHECK(tree.getLetter("100") == 'e');
	CHECK(tree.getLetter("101") == 'f');
	CHECK(tree.getLetter("110") == 'g');
	CHECK(tree.getLetter("111") == 'h');

	CHECK(tree.stringCount("100") == true);
	CHECK(tree.stringCount("1000") == false);
}