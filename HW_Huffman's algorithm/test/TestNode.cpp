#include "../include/HuffmanNode.h"
#include "../src/HuffmanNode.cpp"
#include "doctest.h"

TEST_CASE("test Node functions") {
	HuffmanNode firstNode("a", 2, -1, 5);
	HuffmanNode secondNode("b", -1, 5, 10);
	
	CHECK(firstNode.getWord() == "a");
	CHECK(firstNode.getLeftChild() == 2);
	CHECK(firstNode.getRightChild() == -1);
	CHECK(firstNode.getSize() == 5);
	CHECK((firstNode < secondNode) == true);
}