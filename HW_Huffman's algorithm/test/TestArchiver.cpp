#include "../include/HuffmanArchiver.h"
#include "../src/HuffmanArchiver.cpp"
#include "doctest.h"

#include <iostream>
#include <sstream>

TEST_CASE("test 1 letter to compress") {
	std::stringstream fileInput;
	fileInput << "A";

	std::stringstream fileOutput;
	
	HuffmanArchiver archiver;
	archiver.compress(fileInput, fileOutput);

	int counterLetters, counterTypes;

	fileOutput >> counterLetters >> counterTypes;
	CHECK(counterLetters == 1);
	CHECK(counterTypes == 1);

	unsigned char skipSpace[1];
	fileOutput.read((char*)skipSpace, sizeof(char));

	unsigned char letter[1];
	fileOutput.read((char*)letter, sizeof(char));
	CHECK(letter[0] == 'A');

	int sizeLetter;
	fileOutput >> sizeLetter;
	CHECK(sizeLetter == 1);

	CHECK(!fileOutput.eof());

	fileOutput.read((char*)skipSpace, sizeof(char));
	CHECK(!fileOutput.eof());

	fileOutput.read((char*)letter, sizeof(char));
	CHECK(letter[0] == (char)0);

	fileOutput.read((char*)letter, sizeof(char));
	CHECK(fileOutput.eof());

	CHECK(fileOutput.str().size() == 9);
}

TEST_CASE("test 1 letter to extract") {
	std::stringstream fileInput;
	fileInput << "1 1 A 1  ";

	std::stringstream fileOutput;

	HuffmanArchiver archiver;

	archiver.extract(fileInput, fileOutput);

	CHECK(fileOutput.str().size() == 1);
	CHECK(fileOutput.str() == "A");
}

TEST_CASE("test empty file to compress") {
	std::stringstream fileInput;
	std::stringstream fileOutput;

	HuffmanArchiver archiver;

	archiver.compress(fileInput, fileOutput);

	CHECK(fileOutput.str().size() == 0);
}

TEST_CASE("test empty file to extract") {
	std::stringstream fileInput;
	std::stringstream fileOutput;

	HuffmanArchiver archiver;

	archiver.extract(fileInput, fileOutput);

	CHECK(fileOutput.str().size() == 0);
}

TEST_CASE("test file to compess") {
	std::stringstream fileInput;
	fileInput << "abcd";

	std::stringstream fileOutput;

	HuffmanArchiver archiver;
	archiver.compress(fileInput, fileOutput);

	int counterLetters, counterTypes;

	fileOutput >> counterLetters >> counterTypes;
	CHECK(counterLetters == 4);
	CHECK(counterTypes == 4);

	unsigned char skipSpace[1];
	fileOutput.read((char*)skipSpace, sizeof(char));

	unsigned char letter[1];
	int sizeLetter;

	fileOutput.read((char*)letter, sizeof(char));
	CHECK(letter[0] == 'a');
	fileOutput >> sizeLetter;
	CHECK(sizeLetter == 1);
	fileOutput.read((char*)skipSpace, sizeof(char));

	fileOutput.read((char*)letter, sizeof(char));
	CHECK(letter[0] == 'b');
	fileOutput >> sizeLetter;
	CHECK(sizeLetter == 1);
	fileOutput.read((char*)skipSpace, sizeof(char));

	fileOutput.read((char*)letter, sizeof(char));
	CHECK(letter[0] == 'c');
	fileOutput >> sizeLetter;
	CHECK(sizeLetter == 1);
	fileOutput.read((char*)skipSpace, sizeof(char));

	fileOutput.read((char*)letter, sizeof(char));
	CHECK(letter[0] == 'd');
	fileOutput >> sizeLetter;
	CHECK(sizeLetter == 1);
	fileOutput.read((char*)skipSpace, sizeof(char));

	fileOutput.read((char*)letter, sizeof(char));
	CHECK(letter[0] == (char)27);

	fileOutput.read((char*)letter, sizeof(char));
	CHECK(!fileOutput.eof());
	CHECK(letter[0] == (char)0);
	CHECK(fileOutput.str().size() == 22);

	fileOutput.read((char*)letter, sizeof(char));
	CHECK(fileOutput.eof());
}


TEST_CASE("test file to extract") {
	std::stringstream fileInput;
	fileInput << "4 4 a 1 b 1 c 1 d 1 ";
	char esc = 27;
	fileInput << esc << (char)0;

	std::stringstream fileOutput;

	HuffmanArchiver archiver;
	archiver.extract(fileInput, fileOutput);

	CHECK(fileOutput.str().size() == 4);
	CHECK(fileOutput.str() == "abcd");
}