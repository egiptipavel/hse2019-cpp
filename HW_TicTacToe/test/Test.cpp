#include "Test.h"

#include <cstddef>
#include <cstdio>

int Test::failedNum = 0;
int Test::totalNum = 0;

void Test::check(bool expr, const char* func, const char* filename, size_t lineNum) {
    if (!expr) {
        fprintf(stderr, "FAILED: %s at %s:%ld\n", func, filename, lineNum);
        failedNum++;
    }
    totalNum++;
}

bool Test::showFinalResult() {
    printf("All tests: %d\n", totalNum);
    printf("Tests failed: %d\n", failedNum);
    return failedNum == 0;
}