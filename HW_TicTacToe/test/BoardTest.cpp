#include "Board.h"
#include "BoardTest.h"
#include "Test.h"

#define DO_CHECK(expr) check(expr, __func__, __FILE__, __LINE__);

const size_t size = 10;

void BoardTest::testMove() {
    Board b;
    b.move(0, 0, Player::X);
    DO_CHECK(b.table(0, 0) == 'X');
}

void BoardTest::testGameInProgress() {
    Board b;
    b.move(0, 0, b.turn());
    b.move(0, 1, b.turn());
    b.move(1, 0, b.turn());
    b.move(4, 5, b.turn());
    b.move(5, 5, b.turn());
    DO_CHECK(b.getState() == State::GAME_IN_PROGRESS);
}

void BoardTest::testCanMove() {
    Board b;
    b.move(5, 5, b.turn());
    DO_CHECK(b.canMove(6, 6) == true);
}

void BoardTest::testXwinsHorizontal() {
    Board b;
    b.move(0, 0, Player::X);
    b.move(0, 1, Player::X);
    b.move(0, 2, Player::X);
    b.move(0, 3, Player::X);
    b.move(0, 4, Player::X);
    DO_CHECK(b.getState() == State::X_WINS);
}

void BoardTest::testOwinsHorizontal() {
    Board b;
    b.move(0, 0, Player::O);
    b.move(0, 1, Player::O);
    b.move(0, 2, Player::O);
    b.move(0, 3, Player::O);
    b.move(0, 4, Player::O);
    DO_CHECK(b.getState() == State::O_WINS);
}

void BoardTest::testDraw() {
    Board b;
    for (size_t i = 0; i < size; i++) {
        for (size_t j = 0; j < size; j += 2) {
            if (i % 2 == 0) {
                if (j % 4 == 0) {
                    b.move(j, i, Player::O);
                    b.move(j + 1, i, Player::O);
                    continue;
                } else {
                    b.move(j, i, Player::X);
                    b.move(j + 1, i, Player::X);
                    continue;
                }
            }
            if (i % 2 == 1) {
                if (j % 4 == 0) {
                    b.move(j, i, Player::X);
                    b.move(j + 1, i, Player::X);
                    continue;
                } else {
                    b.move(j, i, Player::O);
                    b.move(j + 1, i, Player::O);
                    continue;
                }
            }
        }
    }
    DO_CHECK(b.getState() == State::DRAW);
}

void BoardTest::testXwinsVertical() {
    Board b;
    b.move(0, 0, Player::X);
    b.move(1, 0, Player::X);
    b.move(2, 0, Player::X);
    b.move(3, 0, Player::X);
    b.move(4, 0, Player::X);
    DO_CHECK(b.getState() == State::X_WINS);
}

void BoardTest::testOwinsVertical() {
    Board b;
    b.move(0, 0, Player::O);
    b.move(1, 0, Player::O);
    b.move(2, 0, Player::O);
    b.move(3, 0, Player::O);
    b.move(4, 0, Player::O);
    DO_CHECK(b.getState() == State::O_WINS);
}

void BoardTest::testXturn() {
    Board b;
    b.move(0, 0, b.turn());
    b.move(0, 1, b.turn());
    b.move(0, 2, b.turn());
    DO_CHECK(b.turn() == Player::X);
}

void BoardTest::testOturn() {
    Board b;
    b.move(0, 0, b.turn());
    b.move(0, 1, b.turn());
    b.move(0, 2, b.turn());
    b.move(0, 3, b.turn());
    DO_CHECK(b.turn() == Player::O);
}

void BoardTest::testXwinsDiagonal() {
    Board b;
    b.move(0, 0, Player::X);
    b.move(1, 1, Player::X);
    b.move(2, 2, Player::X);
    b.move(3, 3, Player::X);
    b.move(4, 4, Player::X);
    DO_CHECK(b.getState() == State::X_WINS);
}

void BoardTest::testOwinsDiagonal() {
    Board b;
    b.move(9, 9, Player::O);
    b.move(8, 8, Player::O);
    b.move(7, 7, Player::O);
    b.move(6, 6, Player::O);
    b.move(5, 5, Player::O);
    DO_CHECK(b.getState() == State::O_WINS);
}

void BoardTest::testBadMove() {
    Board b;
    b.move(7, 7, b.turn());
    b.move(8, 8, b.turn());
    b.move(6, 6, b.turn());
    DO_CHECK(b.canMove(7, 7) == false);
}

void BoardTest::testXwins() {
    Board b;
    b.move(1, 1, b.turn());
    b.move(8, 7, b.turn());
    b.move(1, 2, b.turn());
    b.move(8, 6, b.turn());
    b.move(1, 3, b.turn());
    b.move(8, 5 ,b.turn());
    b.move(1, 4, b.turn());
    b.move(1, 5, b.turn());
    b.move(2, 2, b.turn());
    b.move(8, 4, b.turn());
    b.move(3, 3, b.turn());
    b.move(8, 3, b.turn());
    DO_CHECK(b.getState() == State::X_WINS);
}

void BoardTest::testOwins() {
    Board b;
    b.move(5, 5, b.turn());
    b.move(4, 4, b.turn());
    b.move(6, 6, b.turn());
    b.move(3, 3, b.turn());
    b.move(7, 7, b.turn());
    b.move(2, 2, b.turn());
    b.move(8, 8, b.turn());
    b.move(9, 9, b.turn());
    b.move(6, 5, b.turn());
    b.move(1, 1, b.turn());
    b.move(0, 0, b.turn());
    b.move(8, 9, b.turn());
    b.move(5, 6, b.turn());
    b.move(7, 9, b.turn());
    b.move(4, 6, b.turn());
    b.move(6, 9, b.turn());
    b.move(3, 6, b.turn());
    b.move(5, 8, b.turn());
    b.move(2, 6, b.turn());
    DO_CHECK(b.getState() == State::O_WINS);
}

void BoardTest::testNoOneWins() {
    Board b;
    b.move(3, 4, b.turn());
    b.move(4, 3, b.turn());
    b.move(6, 7, b.turn());
    b.move(9, 9, b.turn());
    b.move(1, 1, b.turn());
    b.move(0, 0, b.turn());
    b.move(0, 9, b.turn());
    b.move(0, 1, b.turn());
    b.move(1, 3, b.turn());
    b.move(0, 5, b.turn());
    b.move(0, 8, b.turn());
    b.move(1, 2, b.turn());
    b.move(9, 8, b.turn());
    b.move(9, 6, b.turn());
    b.move(9, 7, b.turn());
    b.move(9, 5, b.turn());
    b.move(8, 4, b.turn());
    b.move(8, 5, b.turn());
    b.move(8, 7, b.turn());
    b.move(8, 9, b.turn());
    b.move(7, 7, b.turn());
    b.move(7, 9, b.turn());
    b.move(4, 7, b.turn());
    DO_CHECK(b.getState() == State::GAME_IN_PROGRESS);
}

void BoardTest::runAllTests() {
    testMove();
    testGameInProgress();
    testCanMove();
    testXwinsHorizontal();
    testOwinsHorizontal();
    testDraw();
    testXwinsVertical();
    testOwinsVertical();
    testXturn();
    testOturn();
    testXwinsDiagonal();
    testOwinsDiagonal();
    testBadMove();
    testXwins();
    testOwins();
    testNoOneWins();
}