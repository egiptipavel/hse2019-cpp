#ifndef BOARDTEST_H_
#define BOARDTEST_H_

#include "Test.h"
#include "Board.h"

class BoardTest : public Test {
private:
    void testMove();
    void testGameInProgress();
    void testCanMove();
    void testXwinsHorizontal();
    void testOwinsHorizontal();
    void testDraw();  
    void testXwinsVertical();
    void testOwinsVertical();
    void testXturn();
    void testOturn();
    void testXwinsDiagonal();
    void testOwinsDiagonal();
    void testBadMove();
    void testXwins();
    void testOwins();
    void testNoOneWins();
public:
    void runAllTests() override;
};
#endif