#ifndef STDIOBOARDVIEW_H_
#define STDIOBOARDVIEW_H_

#include "Board.h"

class StdioBoardView {
private:
    Board *_board;
    int x, y;
public:
    StdioBoardView(Board *board);

    void runGame(int mode);

    void printGame() const;

    void printWinner() const;

    bool checkInput();

    char getNowPlayer() const;
};
#endif