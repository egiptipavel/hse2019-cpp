#ifndef BOARD_H_
#define BOARD_H_

#include <cstddef>

enum class State {
    GAME_IN_PROGRESS,
    X_WINS,
    O_WINS,
    DRAW,
};

enum class Player {
    X,
    O
};

class Board {
private:
    char** _board;
    int _occupied_cells;
    Player _player;
    State _game_status;
    void checkCell(const char cell);
    bool checkStraight(int x, int y, int mode_first, int mode_second) const;
public:
    Board();
    ~Board();

    Board(const Board&) = delete;

    Board& operator=(const Board&) = delete;

    char table(int x, int y) const;

    Player turn() const;

    bool canMove(int x, int y) const;

    void move(int x, int y, Player);

    void stateGame();

    State getState();
};
#endif