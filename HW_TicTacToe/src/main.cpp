#include "Board.h"
#include "StdioBoardView.h"

#include <string.h>

int main(int argc, char* argv[]) {
    Board map;
    StdioBoardView view(&map);
    if (argc > 1) {
        if (!strcmp(argv[1], "silent")) {
            view.runGame(1);
        }
    } else {
        view.runGame(0);
    }
}