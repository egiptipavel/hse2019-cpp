#include "StdioBoardView.h"

#include <cstdio>
#include <cstdlib>
#include <string.h>
#include <cctype>

const size_t size = 10;
const int coordinates = 2;

bool StdioBoardView::checkInput() {
    bool numbers[coordinates];
    for (int i = 0; i < coordinates; i++) {
        char* str;
        bool letter = false;
        scanf("%ms", &str);
        size_t length = strlen(str);
        for (size_t i = 0; i < length; i++) {
            if (str[i] == '-' && i == 0 && length != 1) {
                continue;
            }
            if (str[i] - '0' < 0 || str[i] - '0' > 9) {
                letter = true;
            }
        }
        if (letter) {
            numbers[i] = false;
        } else {
            numbers[i] = true;
            i == 0 ? y = atoi(str) : x = atoi(str);
        }
        free(str);
    }
    return numbers[0] && numbers[1];
}

char StdioBoardView::getNowPlayer() const {
    return (_board->turn() == Player::O) ? 'O' : 'X';
}

StdioBoardView::StdioBoardView(Board *board) 
    : _board(board)
    {}

void StdioBoardView::runGame(int mode) {
    if (!mode) printGame();
    printWinner();
    while (_board->getState() == State::GAME_IN_PROGRESS) {
        if (!checkInput()) {
            printf("Bad move!\n%c move: ", getNowPlayer());
            continue;
        }
        if (x == -1 && y == -1) { return; }
        if (!_board->canMove(x, y)) {
            printf("Bad move!\n%c move: ", getNowPlayer());
            continue;
        }
        _board->move(x, y, _board->turn());
        _board->stateGame();
        if (!mode) printGame();
        if (mode && _board->getState() != State::GAME_IN_PROGRESS) {
            printGame();
            printWinner();
            return;
        }
        printWinner();
    }
}

void StdioBoardView::printGame() const {
    printf("\n");
    for (std::size_t i = 0; i < size; i++) {
        for (std::size_t j = 0; j < size; j++) {
            printf("%c", _board->table(i, j));
        }
        printf("\n");
    }
}

void StdioBoardView::printWinner() const {
    State status = _board->getState();
    if (status == State::GAME_IN_PROGRESS) {
        printf("%c move: ", getNowPlayer());
        return;
    }
    if (status == State::DRAW) {
        printf("Draw.");
        return;
    }
    printf("%c wins!\n", status == State::X_WINS ? 'X' : 'O');
}bool correct_input();