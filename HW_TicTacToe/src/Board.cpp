#include "Board.h"

#include <cassert>
#include <string.h>
#include <cstdio>

const size_t Field_size = 10;

Board::Board() : _board(new char*[Field_size]) {
    for (size_t i = 0; i < Field_size; i++) {
        _board[i] = new char[Field_size];
    }
    _occupied_cells = 0;
    _game_status = State::GAME_IN_PROGRESS;
    _player = Player::O;
    for (size_t i = 0; i < Field_size; i++) {
        memset(_board[i], '.', Field_size);
    }
}

Board::~Board() {
    for (size_t i = 0; i < Field_size; i++) {
        delete[] _board[i];
    }
    delete[] _board;
}

char Board::table(int x, int y) const {
    return _board[x][y];
}

Player Board::turn() const {
    return _player;
}

bool Board::canMove(int x, int y) const {
    if (x < 0 || x > 9 || y < 0 || y > 9) {
        return false;
    }
    return _board[y][x] == '.';
}

void Board::move(int x, int y, Player gamer) {
    gamer == Player::O ? _board[y][x] = 'O' : _board[y][x] = 'X';
    gamer == Player::O ? _player = Player::X : _player = Player::O;
    _occupied_cells++;
}

void Board::checkCell(const char cell) {
    cell == 'O' ? _game_status = State:: O_WINS : _game_status = State::X_WINS;
}

bool Board::checkStraight(int i, int j, int first, int second) const {
    return (_board[i][j] ==_board[i + first][j + second] &&
    _board[i + first][j + second] ==_board[i + 2 * first][j + 2 * second] &&
    _board[i + 2 * first][j + 2 * second] == _board[i + 3 * first][j + 3 * second] && //Проверка в зависимости от параметров
    _board[i + 3 * first][j + 3 * second] == _board[i + 4 * first][j + 4 * second] && //либо какой-то прямой, либо какой-то диагонали
    _board[i][j] != '.');
}

void Board::stateGame() {
    if (_occupied_cells == Field_size * Field_size) { //Проверка на ничью
        _game_status = State::DRAW;
        return;
    }
    for (size_t i = 0; i < Field_size; i++) { //Проверка по горизонталям
        for (size_t j = 0; j < 6; j++) {
            if (checkStraight(j, i, 1, 0)) {
               checkCell(_board[j][i]);
               return;
            }
        }
    }
    for (size_t i = 0; i < Field_size; i++) { //Проверка по вертикалям
        for (size_t j = 0; j < 6; j++) {
            if (checkStraight(i, j, 0, 1)) {
                checkCell(_board[i][j]);
                return;
            } 
        }
    }
    for (size_t i = 0; i < 6; i++) { //Проверка по диагоналям с левого верхнего угла в правый нижний
        for (size_t j = 0; j < 6; j++) {
            if (checkStraight(i, j, 1, 1)) {
               checkCell(_board[i][j]);
               return;
            }
        }
    }
    for (size_t i = Field_size - 1; i > 3; i--) { //Проверка по диагоналям из правого верхнего угла в левый нижний
        for (size_t j = 0; j < 6; j++) {
            if (checkStraight(i, j, -1, 1)) {
                checkCell(_board[i][j]);
                return;
            }
        }
    }
    _game_status = State::GAME_IN_PROGRESS;
}

State Board::getState() {
    stateGame();
    return _game_status;
}