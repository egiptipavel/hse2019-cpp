#ifndef LINQ_H_
#define LINQ_H_

#include <utility>
#include <vector>
#include <functional>

namespace linq {
namespace impl {

template<typename T, typename Iter>
class range_enumerator;
template<typename T>
class drop_enumerator;
template<typename T, typename U, typename F>
class select_enumerator;
template<typename T, typename F>
class until_enumerator;
template<typename T, typename F>
class where_enumerator;
template<typename T>
class take_enumerator;

template<typename T>
class enumerator {
public:
  virtual T operator*() = 0; // Получает текущий элемент.
  virtual void operator++() = 0;  // Переход к следующему элементу
  virtual explicit operator bool() = 0;  // Возвращает true, если есть текущий элемент

  auto drop(int count) {
    return drop_enumerator<T>(*this, count);
  }

  template<typename U = T, typename F>
  auto select(F &&func) {
    return select_enumerator<U, T, F>(*this, std::move(func));
  }

 auto until_eq(T end) {
    return until([=](T x) { return x == end; });
  }

  template<typename F>
  auto until(F &&func) {
    return until_enumerator<T, F>(*this, std::move(func));
  }

  auto where_neq(T notEgual) {
    return where([=](T x) { return x != notEgual; });
  }

  template<typename F>
  auto where(F &&func) {
    return where_enumerator<T, F>(*this, std::move(func));
  }

  auto take(int count) {
    return take_enumerator<T>(*this, count);
  }

  template<typename Iter>
  void copy_to(Iter it) {
    while(static_cast<bool>(*this)) {
      std::vector<T> result;
      result.push_back(*(*this));
      ++(*this);
      std::copy(result.begin(), result.end(), it);
    }
  }

  std::vector<T> to_vector() {
    std::vector<T> result;
    while(static_cast<bool>(*this)) {
      result.push_back(*(*this));
      ++(*this);
    }
    return result;
  }
};

template<typename T, typename Iter>
class range_enumerator : public enumerator<T> {
public:
  T operator*() override {
    return *begin_;
  }

  void operator++() override {
    if (*this) begin_++;
  }

  explicit operator bool() override {
    return begin_ != end_;
  }

  range_enumerator(Iter begin, Iter end) : begin_(begin), end_(end) {
  }

private:
  Iter begin_, end_;
};

template<typename T>
class drop_enumerator : public enumerator<T> {
public:
  T operator*() override {
    return *parent_;
  }

  void operator++() override {
    ++parent_;
  }

  explicit operator bool() override {
    while(count_ && static_cast<bool>(parent_)) {
      ++parent_;
      count_--;
    }
    return static_cast<bool>(parent_);
  }

  drop_enumerator(enumerator<T> &parent, int count) : parent_(parent), count_(count) {
  }

private:
  enumerator<T> &parent_;
  int count_;
};

template<typename T, typename U, typename F>
class select_enumerator : public enumerator<T> {
public:
  T operator*() override {
    return func_(static_cast<T>(*parent_));
  }

  void operator++() override {
    ++parent_;
  }

  explicit operator bool() override {
    return static_cast<bool>(parent_);
  }

  select_enumerator(enumerator<U> &parent, F func) : parent_(parent), func_(std::move(func)) {
  }

private:
  enumerator<U> &parent_;
  F func_;
};

template<typename T, typename F>
class until_enumerator : public enumerator<T> {
public:
  T operator*() override {
    return *parent_;
  }

  void operator++() override {
    ++parent_;
  }

  explicit operator bool() override {
    bool exist = static_cast<bool>(parent_);
    return (exist && predicate_(*parent_)) ? false : exist;
  }

  until_enumerator(enumerator<T> &parent, F predicate) : parent_(parent), predicate_(std::move(predicate)) {
  }

private:
  enumerator<T> &parent_;
  F predicate_;
};

template<typename T, typename F>
class where_enumerator : public enumerator<T> {
public:
  T operator*() override {
    return *parent_;
  }

  void operator++() {
    ++parent_;
  }

  explicit operator bool() override {
    while(static_cast<bool>(parent_) && !func_(*parent_)) ++parent_;
    return static_cast<bool>(parent_);
  }

  where_enumerator(enumerator<T> &parent, F func) : parent_(parent), func_(std::move(func)) {
  }

private:
  enumerator<T> &parent_;
  F func_;
};

template<typename T>
class take_enumerator : public enumerator<T> {
public:
  T operator*() override {
    return *parent_;
  }

  void operator++() override {
    ++parent_;
  }

  explicit operator bool() override {
    if (!count_) {
      return false;
    } else {
      count_--;
      return static_cast<bool>(parent_);
    }
  }

  take_enumerator(enumerator<T> &parent, int count) : parent_(parent), count_(count) {
  }

private:
  enumerator<T> &parent_;
  int count_;
};

} // namespace impl

template<typename Iter, typename T = typename std::iterator_traits<Iter>::value_type>
auto from(Iter begin, Iter end) {
  return impl::range_enumerator<T, Iter>(begin, end);
}

} // namespace linq

#endif